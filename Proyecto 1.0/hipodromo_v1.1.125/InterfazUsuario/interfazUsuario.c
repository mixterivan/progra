#include <windows.h>
#include<stdio.h>
#include "../InterfazUsuario/interfazUsuario.h"
#include "../InterfazUsuario/CaballoIU.h"
#include "../InterfazUsuario/interfazGrafica.h"
void inicInterfazUsuario()//imprime los recuadros y el titulo
{
    int n;

    system("mode con cols=90 lines=33");
    setColorTexto(12);

    gotoxy(40,1);
    printf("HIPODROMO");

    //rectangulo superior
    gotoxy(0,0);
    printf("%c", 201);
    gotoxy(89,0);
    printf("%c", 187);
    for(n=1; n<89; n++)//lenea superior horizontal
    {
        gotoxy(n,0);
        printf("%c",205);
    }

    gotoxy(0,3);
    printf("%c", 200);
    for(n=1; n<3; n++)//linea de la derecha vertical
    {
        gotoxy(89,n);
        printf("%c",186);
    }
    for(n=1; n<3; n++)//linea de la izquiera vertical
    {
        gotoxy(0,n);
        printf("%c",186);
    }
    gotoxy(89,3);
    printf("%c", 188);
    for(n=1; n<89; n++)//linea inferior horizontal
    {
        gotoxy(n,3);
        printf("%c",205);
    }

    //rectangulo de la izquierda
    for(n=1; n<89; n++)//lenea superior horizontal
    {
        gotoxy(n,4);
        printf("%c",196);
    }
    for(n=1; n<89; n++)//lenea inferior horizontal
    {
        gotoxy(n,25);
        printf("%c",196);
    }
    gotoxy(0,4);
    printf("%c", 218);
    gotoxy(44,4);
    printf("%c", 191);
    gotoxy(0,25);
    printf("%c", 192);
    gotoxy(44,25);
    printf("%c", 217);
    for(n=5; n<25; n++)//linea de la izquiera vertical
    {
        gotoxy(0,n);
        printf("%c",179);
    }
    for(n=5; n<25; n++)//linea de la derecha vertical
    {
        gotoxy(44,n);
        printf("%c",179);
    }


    //rectangulo de la derecha
    for(n=5; n<25; n++)//linea de la izquiera vertical
    {
        gotoxy(45,n);
        printf("%c",179);
    }
    for(n=5; n<25; n++)//linea de la derecha vertical
    {
        gotoxy(89,n);
        printf("%c",179);
    }
    gotoxy(45,4);
    printf("%c", 218);
    gotoxy(89,4);
    printf("%c", 191);
    gotoxy(45,25);
    printf("%c", 192);
    gotoxy(89,25);
    printf("%c", 217);

    //rectangulo inferior
    for(n=10; n<79; n++)//lenea superior horizontal
    {
        gotoxy(n,27);
        printf("%c",196);
    }
    for(n=10; n<79; n++)//lenea inferior horizontal
    {
        gotoxy(n,31);
        printf("%c",196);
    }
    for(n=28; n<31; n++)//linea de la izquiera vertical
    {
        gotoxy(10,n);
        printf("%c",179);
    }
    for(n=28; n<31; n++)//linea de la derecha vertical
    {
        gotoxy(79,n);
        printf("%c",179);
    }
    gotoxy(10,27);
    printf("%c", 218);
    gotoxy(79,27);
    printf("%c", 191);
    gotoxy(10,31);
    printf("%c", 192);
    gotoxy(79,31);
    printf("%c", 217);

    gotoxy(14,29);

    return;
}

int menuPrincipal()//imprime unas opciones y escanea que opcion a elegido el usuario
{
    int opcion;

    gotoxy(3,6);
    printf("1.Gestion de caballos\n");
    gotoxy(3,8);
    printf("2.Gestion de carreras\n");
    gotoxy(3,10);
    printf("3.Inicio carrera\n");
    gotoxy(3,12);
    printf("0.Fin de programa");
    muestraMensajeInfo("Elige una de las opciones: ");
    scanf("%i", &opcion);

    return opcion;
}

void gestionMenuPrincipal()//en funcion de la opcion elegida va a distintos menus
{
    int opcion;

    opcion = menuPrincipal();

    switch(opcion)
    {
    case 0:

        exit(0);
        break;

    case 1:
        gestionMenuCaballos();

        break;

    case 2:

        system("cls");
        inicInterfazUsuario();
        gotoxy(14,28);
        printf("Esta opcion aun no esta implementada.");
        gestionMenuPrincipal();

        break;

    case 3:

        system("cls");
        inicInterfazUsuario();
        gotoxy(14,28);
        printf("Esta opcion aun no esta implementada.");
        gestionMenuPrincipal();
        break;

    default:

        inicInterfazUsuario();
        gestionMenuPrincipal();
        break;
    }
}
