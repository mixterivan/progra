#ifndef CABALLOSYS_H_INCLUDED
#define CABALLOSYS_H_INCLUDED
#include <stdbool.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdbool.h>
bool altaCaballoSys(long dienuevo, int victorias2, float ganancias2,int edad2,char nombre2[20],char txt[]);
bool generaInformeCaballos();
int cargaListaCaballoSys(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char msgError[],char txt[]);
bool guardaListaCaballosSys(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],int num, char msgError[],char txt[]);
#endif // CABALLOSYS_H_INCLUDED
