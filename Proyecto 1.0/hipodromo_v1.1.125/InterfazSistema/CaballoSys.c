#include <stdbool.h>
#include <stdio.h>
#include "../InterfazSistema/CaballoSys.h"
#include "../AccesoDatos/CaballoAD.h"
#include "../InterfazUsuario/interfazGrafica.h"

int cargaListaCaballoSys(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char msgError[],char txt[]) //comprueba que se estan pasando bien los datos
{
    int num;
    num = cargaListaCaballosAD(die, victorias, ganancias,edad,nombre, msgError,txt);
    if (num==-1)
    {
        muestraMensajeInfo(msgError);
        return -1;
    }
    return num;
}

bool altaCaballoSys(long dienuevo, int victorias2, float ganancias2,int edad2,char nombre2[20],char txt[]) //recoge los datos de altacaballo ad
{
    altaCaballoAD( dienuevo,victorias2, ganancias2,edad2,nombre2,txt);
    return true;
}

bool generaInformeCaballos() //escanea los caballos de caballos txt y los imprime en infocaballos.tzt
{
    long die;
    int victorias,edad;
    float ganancias;
    char nombre[20];

    FILE* ptr1;
    FILE* ptr2;

    ptr1 = fopen("caballos.txt","rt");
    ptr2 = fopen("infoCaballos.txt","wt");

    if(ptr1 == NULL)//comprobacion de que el archivo se abre correctamente
    {
        gotoxy(14,29);
        printf("Error al abrir el archivo caballos.txt");
        return false;
    }

    if(ptr2 == NULL)
    {
        gotoxy(14,29);
        printf("Error al abrir el archivo infoCaballos.txt");
        return false;
    }

    fprintf(ptr2,"_______________________________________________\n");
    fprintf(ptr2,"DIE    Victorias    Ganancias   Edad    Nombre \n");
    fprintf(ptr2,"_______________________________________________\n");

    while(feof(ptr1)==0)//se utiliza para comprobar cuando se ha llegado al final del archivo
    {
        fscanf(ptr1,"%ld", &die);
        fscanf(ptr1,"%i", &victorias);
        fscanf(ptr1,"%f",&ganancias);
        fscanf(ptr1,"%i", &edad);
        fscanf(ptr1,"%s", nombre);


        escribeCaballoTXT(die,victorias,ganancias,edad,nombre,ptr2);
    }

    fclose(ptr1);
    fclose(ptr2);

    return true;
}

bool guardaListaCaballosSys(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],int num, char msgError[],char txt[])//recoge los datos de guardalistacaballoSys
{
    num=guardaListaCaballosDAT(die,victorias,ganancias,edad,nombre,num,txt);
    if(num == false)
    {
        muestraMensajeInfo(msgError);
        return false;
    }
    return true;
}
