#ifndef INTERFAZGRAFICA_H_INCLUDED
#define INTERFAZGRAFICA_H_INCLUDED
#include <stdio.h>
void setColorTexto(int colors);
void muestraMensajeInfo(char *msg);
void gotoxy(int x, int y);
int leerOpcionValida(char *msg, int num);
#endif // INTERFAZGRAFICA_H_INCLUDED
