#include <stdio.h>
#include <windows.h>
#include "../InterfazUsuario/interfazGrafica.h"

void gotoxy(int x, int y)// funcion para indicar donde realizar acciones
{
    COORD coord;

    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    return;
}

void setColorTexto(int colors)//determina el color de las letras y el fondo
{
    HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, colors);
    system("color F0");
}

void muestraMensajeInfo(char *msg)//imprime un mensaje en la misma posicion
{
    gotoxy(14,29);
    printf("%s", msg);
}

int leerOpcionValida(char *msg, int num)//pide un numero lo escanea y comprueba que este dentro de las posibles opciones del menu
{
    int numeroDado;

    while(1)
    {

        gotoxy(7,21);
        printf("%s",msg);
        scanf("%i",&numeroDado);

        if((numeroDado <= num) && (numeroDado>=0))
        {
            return numeroDado;
        }
    }
}

void borrarinferior()//imprime espacion en blanco en el recuadro inferior
{
    gotoxy(14,29);
    printf("                                            ");
}
