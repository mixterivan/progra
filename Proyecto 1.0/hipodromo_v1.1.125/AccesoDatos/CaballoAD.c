#include <stdbool.h>
#include <stdio.h>
#include "../AccesoDatos/CaballoAD.h"
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/interfazGrafica.h"

int cargaListaCaballosAD(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char msgError[],char txt[])
{
    int i=0;
    FILE* ptr;

    ptr = fopen(txt,"rt"); //Abre el archivo caballos.txt

    if(ptr == NULL)//comprobacion de que el archivo se abre correctamente
    {
        sprintf(msgError,"Error al abrir el archivo .txt");
        return -1;
    }

    while(fscanf(ptr,"%ld", &die[i])==1) //lee todas las lineas del archivo caballos.txt hasta que no quedan mas y recoge en i el numero tota de ineas
    {
        fscanf(ptr,"%d %f %d  %s",&victorias[i], &ganancias[i],&edad[i] ,nombre[i]);
        i++;
    }
    fclose(ptr);
    return i;
}

void escribeCaballoTXT( long die, int victorias, float ganancias,int edad,char nombre[], FILE *ptr) //imprime el caballo recibido
{
    fprintf(ptr,"%3ld    %9.2i   %-10f     %9.2i  %6s\n", die, victorias, ganancias,edad,nombre);  //Escribe los caballos guardados en el archivo de texto caballos
}

bool altaCaballoAD( long die, int victorias, float ganancias,int edad,char nombre[],char txt[]) //pide cosas nuevas y las imprime
{
    FILE *ptr;

    ptr = fopen(txt,"at");

    if(ptr == NULL)//comprobacion de que el archivo se abre
    {
        muestraMensajeInfo("El archivo no se ha podido abrir correctamente");

        return false;
    }
    else
    {
        fprintf(ptr, "%6ld\t  %d\t   %.2f\t %d\t %s\n",die, victorias, ganancias,edad, nombre);
        fclose(ptr);

        return true;
    }
}

bool guardaListaCaballosDAT(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20], int num,char txt[]) //escribe uno por uno todos los caballos de nuevo
{
    FILE* ptr;
    ptr = fopen(txt,"wt");
    int i=0;
    if (ptr == NULL)
    {
        muestraMensajeInfo("El archivo no se ha podido abrir correctamente");
        return false;
    }
    while(i<num)
    {
        escribeCaballoTXT(die[i],victorias[i],ganancias[i],edad[i],nombre[i],ptr);
        i++;
    }
    fclose(ptr);
    return true;
}
