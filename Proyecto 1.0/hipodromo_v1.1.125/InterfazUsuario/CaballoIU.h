#ifndef CABALLOIU_H_INCLUDED
#define CABALLOIU_H_INCLUDED
#include <stdio.h>
#include "stdbool.h"
void gestionMenuCaballos();
void ListadoCaballos(char txt[]);
void comparaCaballo();
void muestraCaballo( long die, int victorias, float ganancias,int edad,char nombre[]);
void actualizaCaballoIU();
void bajaCaballoIU();
void caballoMasRentableIU();
void caballoMasVictoriosoIU();
void muestraListaCaballos(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char txt1[]);
void fusionaListaCaballos();
void caballosporedad();
#endif // CABALLOIU_H_INCLUDED
