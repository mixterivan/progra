#ifndef CABALLOAD_H_INCLUDED
#define CABALLOAD_H_INCLUDED
#include <stdbool.h>
#include <stdio.h>
void escribeCaballoTXT( long die, int victorias, float ganancias,int edad,char nombre[], FILE *ptr);
int cargaListaCaballosAD(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char msgError[],char txt[]);
bool altaCaballoAD( long die, int victorias, float ganancias,int edad,char nombre[],char txt[]);
bool guardaListaCaballosDAT(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20], int num,char txt[]);
#endif // CABALLOAD_H_INCLUDED
