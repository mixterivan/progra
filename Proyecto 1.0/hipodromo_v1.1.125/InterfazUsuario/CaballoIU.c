#include <stdio.h>
#include "stdbool.h"
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include "../InterfazUsuario/CaballoIU.h"
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/interfazUsuario.h"
#include "../InterfazSistema/CaballoSys.h"


void altacaballoIU() //imprime mensajes que solicitan informacion al usuario y escanea los datos que este introduce
{
    long dienuevo;
    int victorias2,edad2;
    float ganancias2;
    char nombre2[20],txt1[20];
    system("cls");
    inicInterfazUsuario();

    gotoxy(2,8);
    printf("Archivos disponibles:");
    gotoxy(2,9);
    printf("caballos.txt");
    gotoxy(2,10);
    printf("caballos1.txt");
    gotoxy(2,11);
    printf("caballos2.txt");
    gotoxy(2,12);
    printf("caballos3.txt");
    gotoxy(2,13);
    printf("caballosplus.txt");

    muestraMensajeInfo("Teclee el nombre del archivo donde quiera dar de alta: ");
    scanf("%s",txt1);
    system("cls");
    inicInterfazUsuario();

    muestraMensajeInfo("                                           ");
    muestraMensajeInfo("Introduce el nuevo DIE: ");
    scanf("%ld",&dienuevo);
    gotoxy(46,9);
    printf("DIE: %ld", dienuevo);

    muestraMensajeInfo("                                           ");
    muestraMensajeInfo("Introduce las victorias: ");
    scanf("%i",&victorias2);
    gotoxy(46,10);
    printf("Victorias: %i", victorias2);

    muestraMensajeInfo("                                           ");
    muestraMensajeInfo("Introduce las ganancias: ");
    scanf("%f",&ganancias2);
    gotoxy(46,11);
    printf("Ganancias: %f", ganancias2);

    muestraMensajeInfo("                                           ");
    muestraMensajeInfo("Introduce el nombre: ");
    scanf("%s",nombre2);
    gotoxy(46,12);
    printf("Nombre: %s", nombre2);

    muestraMensajeInfo("                                           ");
    muestraMensajeInfo("Introduce la edad: ");
    scanf("%i",&edad2);
    gotoxy(46,13);
    printf("Edad: %i", edad2);

    altaCaballoSys(dienuevo, victorias2, ganancias2,edad2,nombre2,txt1);

    gestionMenuCaballos();
}

int MenuCaballos() //un menu y recoge que opcion del menu ha seleccionado el usuario
{
    system("cls");
    inicInterfazUsuario();
    int opcion;
    gotoxy(3,9);
    printf("1.Listado de caballos\n");

    gotoxy(3,10);
    printf("2.Informe de caballos\n");

    gotoxy(3,11);
    printf("3.Alta de un caballo\n");

    gotoxy(3,12);
    printf("4.Actualizar caballos\n");

    gotoxy(3,13);
    printf("5.Baja de un caballo\n");

    gotoxy(3,14);
    printf("6.Caballo mas rentable\n");

    gotoxy(3,15);
    printf("7.Caballo con mas victorias\n");

    gotoxy(3,16);
    printf("8.Fusiona listas de caballos\n");

    gotoxy(3,17);
    printf("9.Fusiona caballos\n");

    gotoxy(3,18);
    printf("10.Clasifica caballos\n");

    gotoxy(3,19);
    printf("11.Caballos por edad\n");

    gotoxy(3,20);
    printf("0.Menu anterior");
    borrarinferior();

    muestraMensajeInfo("Elige una de las opciones: ");
    scanf("%i",&opcion);


    return opcion;
}

void muestraCaballo( long die, int victorias, float ganancias,int edad,char nombre[]) //muestra los datos de un caballo
{
    printf("%6ld    %3i    %9.2f  %3i  %-10s",die, victorias, ganancias,edad, nombre);
}

void ListadoCaballos(char txt[]) //imprime los datos de todos los caballos guardados en caballos.txt
{
    long die[50];
    int victorias[50],edad[20];
    float ganancias[50];
    char nombre[20][20], msg [100];
    int i;
    int j=10;
    int num;


    system("cls");
    inicInterfazUsuario();

    gotoxy(3,6);
    printf("Listado de caballos");

    muestraMensajeInfo("Presione la tecla intro para continuar.");

    num = cargaListaCaballoSys( die, victorias, ganancias,edad,nombre,msg,txt);

    if (num== -1)
    {
        muestraMensajeInfo(msg);
        exit(1);
    }
    else
    {

        gotoxy(2,8);
        printf("DIE   Victorias  Ganancias  Edad Nombre\n\n");

        for (i = 0; i<num; i++)//recorre todas las filas hasta que se acaban los caballos
        {
            gotoxy(1,j);
            muestraCaballo(die[i], victorias[i], ganancias[i],edad[i],nombre[i]);
            j++;
        }
        getchar();
    }
    getchar();
    return;
}

void bajaCaballoIU() //elimina un caballo y reordena todos para no dejar un espacio en blanco
{
    int num;
    int i;
    int j;
    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    char msgError[50];
    long diebaja;
    char encontrado;
    char txt1[50]= "caballosplus.txt";
    char txt2[50]= "caballosplus.txt";

    num = cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    ListadoCaballos(txt1);
    muestraMensajeInfo("Teclee el DIE del caballo que quiere dar de baja: ");
    scanf("%ld",&diebaja);

    for(i=0; i<num; i++)
    {
        if(die[i] == diebaja)
        {
            encontrado=true;

            for(j=i; j<num-1; j++)// sube todos los caballos a partir del caballo a eliminar una posicion hacia arriba y resta uno al numero total de caballos
            {
                die[j] = die[j+1];
                strcpy(nombre[j],nombre[j+1]);
                victorias[j] = victorias[j+1];
                ganancias[j] = ganancias[j+1];
                edad[j]= edad[j+1];
            }
            num --;
            break;
        }

    }
    if(encontrado)
    {
        guardaListaCaballosSys(die,victorias,ganancias,edad,nombre,num,msgError,txt2);
        ListadoCaballos(txt1);
        muestraMensajeInfo("Caballo dado de baja con exito");
    }

    else
    {
        muestraMensajeInfo("El caballo no esta registrado");
        exit(1);
    }
}

void actualizaCaballoIU()//modifica las ganancias de un caballo y le suma una victoria
{
    int num;
    int i;
    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    char msgError[50];
    char nombrewin[10];
    float premiob;
    char encontrado=false;
    char txt1[50]= "caballosplus.txt";
    char txt2[50]= "caballosplus.txt";

    ListadoCaballos(txt1);

    muestraMensajeInfo("Teclee el nombre del caballo ganador: ");
    scanf("%s",nombrewin);
    borrarinferior();
    muestraMensajeInfo("Teclee el premio obtenido: ");
    scanf("%f",&premiob);

    num= cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    if(num==-1)
    {
        muestraMensajeInfo("No se ha podido cargar la lista de caballos");
        return;
    }
    for(i=0; i<num; i++)
    {
        if(strcmp(nombre[i],nombrewin)==0) //modifica los datos del caballo seleccionado sumandole el premio a las ganancias y sumandole una victoria
        {
            encontrado= true;
            victorias[i]= victorias[i]+1;
            ganancias[i]+= premiob;
            break;
        }
    }
    if(encontrado)
    {
        guardaListaCaballosSys(die,victorias,ganancias,edad,nombre,num,msgError,txt2); //guarda el caballo y lo muestra junto a los demas
        ListadoCaballos(txt1);
        muestraMensajeInfo("Caballo actualizado con exito");

    }
    else
    {
        muestraMensajeInfo("El caballo no esta registrado");
        exit(1);
    }
}

void caballoMasRentableIU()
{
    int num;
    int i;
    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    char msgError[50];
    float gananciamax=0;
    int linea;
    char cabren[50];
    char txt1[50]= "caballosplus.txt";



    num = cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    if(num==-1)
    {
        muestraMensajeInfo("No se ha podido cargar la lista de caballos");
        return;
    }
    for(i=0; i<num; i++) //recorre todos los caballos y si encuentra uno con mas ganancias guarda ese dato en la variable si no tiene mas pasa al siguiente, cuando llega al final se recoge en que linea se encuentra el caballo
    {
        if(ganancias[i]>gananciamax)
        {
            gananciamax=ganancias[i];
            linea=i;

        }
    }
    system("cls");
    inicInterfazUsuario();
    gotoxy(2,8);
    printf(" DIE   Victorias  Ganancias  Nombre    \n\n");
    gotoxy(3,10);
    muestraCaballo(die[linea],victorias[linea],ganancias[linea],edad[linea],nombre[linea]); //indica el caballo con mas ganancias
    sprintf(cabren,"El caballo mas rentable es %s",nombre[linea]);
    muestraMensajeInfo(cabren);
    gotoxy(14,30);
    system("pause");

}

void caballoMasVictoriosoIU() //lo mismo que el de ganancias pero con victorias
{
    int num;
    int i;
    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    char msgError[50];
    float victoriamax =0 ;
    int linea;
    char cabvic[50];
    char txt1[50]= "caballosplus.txt";


    num = cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    if(num==-1)
    {
        muestraMensajeInfo("No se ha podido cargar la lista de caballos");
        return;
    }
    for(i=0; i<num; i++)
    {
        if(victorias[i]>victoriamax)
        {
            victoriamax=victorias[i];
            linea=i;

        }
    }
    system("cls");
    inicInterfazUsuario();
    gotoxy(2,8);
    printf(" DIE   Victorias  Ganancias  Edad Nombre\n\n");
    gotoxy(3,10);
    muestraCaballo(die[linea],victorias[linea],ganancias[linea],edad[linea],nombre[linea]);

    sprintf(cabvic,"El caballo mas victorioso es %s",nombre[linea]);
    muestraMensajeInfo(cabvic);
    gotoxy(14,30);
    system("pause");


}

void muestraListaCaballos(long die[], int victorias[], float ganancias[],int edad[],char nombre[][20],char txt1[]) //imprime los datos de todos los caballos guardados en caballos.txt
{
    char msg[50]="Error";
    int i;
    int j=10;
    int num;



    system("cls");
    inicInterfazUsuario();

    gotoxy(3,6);
    printf("Listado de caballos");

    muestraMensajeInfo("Presione la tecla intro para continuar.");

    borrarinferior();

    num = cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msg,txt1);

    if (num== -1)
    {
        muestraMensajeInfo(msg);
        exit(1);
    }
    else
    {

        gotoxy(2,8);
        printf("DIE   Victorias  Ganancias  Edad Nombre\n\n");

        for (i = 0; i<num; i++)//recorre todas las filas hasta que se acaban los caballos
        {
            gotoxy(1,j);
            muestraCaballo(die[i], victorias[i], ganancias[i],edad[i],nombre[i]);
            j++;
        }
    }
    return;
}

void fusionaListaCaballos()
{
    int num1;
    int num2;
    int num3=0;
    int i;
    int j;
    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    char nombre2[20][20];
    long die2[20];
    int victorias2[20],edad2[20];
    float ganancias2[20];
    char nombre3[20][20];
    long die3[20];
    int victorias3[20],edad3[20];
    float ganancias3[20];
    char msgError[50]="Error";
    char txt1[50];
    char txt2[50];

    system("cls");
    inicInterfazUsuario();

    gotoxy(2,8);
    printf("Archivos disponibles:");
    gotoxy(2,9);
    printf("caballos.txt");
    gotoxy(2,10);
    printf("caballos1.txt");
    gotoxy(2,11);
    printf("caballos2.txt");
    gotoxy(2,12);
    printf("caballos3.txt");

    muestraMensajeInfo("Teclee el nombre del primer archivo: ");
    scanf("%s",txt1);
    num1= cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    system("cls");
    inicInterfazUsuario();
    if (num1== -1)
    {
        muestraMensajeInfo(msgError);
        exit(1);
    }

    ListadoCaballos(txt1);
    system("cls");
    inicInterfazUsuario();

    gotoxy(2,8);
    printf("Archivos disponibles:");
    gotoxy(2,9);
    printf("caballos.txt");
    gotoxy(2,10);
    printf("caballos1.txt");
    gotoxy(2,11);
    printf("caballos2.txt");
    gotoxy(2,12);
    printf("caballos3.txt");
    muestraMensajeInfo("Teclee el nombre del segundo archivo: ");
    scanf("%s",txt2);



    num2= cargaListaCaballoSys(die2,victorias2,ganancias2,edad2,nombre2,msgError,txt2);

    if (num2== -1)
    {
        muestraMensajeInfo(msgError);
        exit(1);
    }
    ListadoCaballos(txt2);
    system("cls");
    inicInterfazUsuario();




    for(j=0; j<num2; j++)
    {
        for(i=0; i<num1; i++)
        {
            if(die2[j]==die[i])
            {
                die3[num3]=die[j];
                victorias3[num3]=victorias2[j]+=victorias[i];
                ganancias3[num3]=ganancias2[j]+=ganancias[i];
                edad3[num3]=edad2[j]+=edad[i];
                strcpy(nombre3[num3],nombre[j]);
                num3++;
                break;
            }

        }

    }
    guardaListaCaballosSys(die3,victorias3,ganancias3,edad3,nombre3,num3,msgError,"fusioncaballos.txt");
    muestraListaCaballos(die3,victorias3,ganancias3,edad3,nombre3,"fusioncaballos.txt");
    muestraMensajeInfo("Listas fusionadas con exito");
    getchar();

}

void comparaCaballo()
{
    int num;
    int num2=0;

    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];

    char nombre2[20][20];
    long die2[20];
    int victorias2[20],edad2[20];
    float ganancias2[20];

    char msgError[50];
    int i;
    int j=0;
    int victoriasupp;


    system("cls");
    inicInterfazUsuario();



    muestraMensajeInfo("Teclee las victorias: ");
    scanf("%i",&victoriasupp);

    num = cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,"caballos.txt");

    if(num==-1)
    {
        muestraMensajeInfo("No se ha podido cargar la lista de caballos");
        return;
    }

    for(i=0; i<num; i++)
    {
        if(victorias[i]>=victoriasupp)
        {
            die2[j]=die[i];
            victorias2[j]=victorias[i];
            ganancias2[j]=ganancias[i];
            edad2[j]=edad[i];
            strcpy(nombre2[j],nombre[i]);
            j++;
            num2++;

        }

    }
    guardaListaCaballosSys(die2,victorias2,ganancias2,edad2,nombre2,num2,msgError,"caballos2.txt");
    ListadoCaballos("caballos2.txt");
}

void fusionaCaballos()
{
    int num1;
    int num2;

    int i;
    int j;

    char nombre[20][20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];

    char nombre2[20][20];
    long die2[20];
    int victorias2[20],edad2[20];
    float ganancias2[20];

    char msgError[50]="Error";
    char txt1[50];
    char txt2[50];

    system("cls");
    inicInterfazUsuario();

    gotoxy(2,8);
    printf("Archivos disponibles:");
    gotoxy(2,9);
    printf("caballos.txt");
    gotoxy(2,10);
    printf("caballos1.txt");
    gotoxy(2,11);
    printf("caballos2.txt");
    gotoxy(2,12);
    printf("caballos3.txt");

    muestraMensajeInfo("Teclee el nombre del primer archivo: ");
    scanf("%s",txt1);
    num1= cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,txt1);

    system("cls");
    inicInterfazUsuario();
    if (num1== -1)
    {
        muestraMensajeInfo(msgError);
        exit(1);
    }

    system("cls");
    inicInterfazUsuario();

    gotoxy(2,8);
    printf("Archivos disponibles:");
    gotoxy(2,9);
    printf("caballos.txt");
    gotoxy(2,10);
    printf("caballos1.txt");
    gotoxy(2,11);
    printf("caballos2.txt");
    gotoxy(2,12);
    printf("caballos3.txt");
    muestraMensajeInfo("Teclee el nombre del segundo archivo: ");
    scanf("%s",txt2);


    num2= cargaListaCaballoSys(die2,victorias2,ganancias2,edad2,nombre2,msgError,txt2);

    if (num2== -1)
    {
        muestraMensajeInfo(msgError);
        exit(1);
    }

    system("cls");
    inicInterfazUsuario();

    for(j=0; j<num2; j++)
    {
        for(i=0; i<num1; i++)
        {
            if(die2[j]==die[i])
            {
                victorias[i]+=victorias2[j];
                ganancias[i]+=ganancias2[j];
                break;
            }
        }
        if(die2[j]!=die[i])
        {
            die[num1]=die2[j];
            victorias[num1]=victorias2[j];
            ganancias[num1]=ganancias2[j];
            edad[num1]=edad2[j];
            strcpy(nombre[num1],nombre2[j]);
            num1 ++;
        }

    }

    guardaListaCaballosSys(die,victorias,ganancias,edad,nombre,num1,msgError,"fusionacaballos.txt");
    muestraListaCaballos(die,victorias,ganancias,edad,nombre,"fusionacaballos.txt");
    muestraMensajeInfo("Caballos fusionados con exito");
    getchar();
    getchar();
}
void caballosporedad()
{
    char nombre[20][20],msgError[20];
    long die[20];
    int victorias[20],edad[20];
    float ganancias[20];
    int num,i;

    system("cls");
    inicInterfazUsuario();
    num=cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,"caballosplus.txt");
    muestraListaCaballos(die,victorias,ganancias,edad,nombre,"caballosplus.txt");
    muestraMensajeInfo("Presiona intro para continuar");
    getchar();
    getchar();


     for(i=0; i<num; i++)
     {
         if (edad[i]<=3)
         {
             altaCaballoSys(die[i],victorias[i],ganancias[i],edad[i],nombre[i],"potros.txt");
         }
         else
         {
             altaCaballoSys(die[i],victorias[i],ganancias[i],edad[i],nombre[i],"corceles.txt");
         }
     }
    cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,"potros.txt");
    muestraListaCaballos(die,victorias,ganancias,edad,nombre,"potros.txt");
    muestraMensajeInfo("Presiona intro para continuar");
    getchar();

    cargaListaCaballoSys(die,victorias,ganancias,edad,nombre,msgError,"corceles.txt");
    muestraListaCaballos(die,victorias,ganancias,edad,nombre,"corceles.txt");
    muestraMensajeInfo("Presiona intro para continuar");
    getchar();



}

void gestionMenuCaballos() //lo mismo que gestionmenuprincipal pero con distintas opciones
{
    int opcion;

    opcion=MenuCaballos();

    switch(opcion)
    {
    case 0:

        inicInterfazUsuario();
        gestionMenuPrincipal();
        menuPrincipal();

        break;

    case 1:

        ListadoCaballos("caballosplus.txt");
        gestionMenuCaballos();
        break;

    case 2:

        generaInformeCaballos();
        gestionMenuCaballos();
        break;

    case 3:

        altacaballoIU();
        gestionMenuCaballos();
        break;

    case 4:

        actualizaCaballoIU();
        gestionMenuCaballos();
        break;

    case 5:

        bajaCaballoIU();
        gestionMenuCaballos();
        break;

    case 6:
        caballoMasRentableIU();
        gestionMenuCaballos();
        break;

    case 7:
        caballoMasVictoriosoIU();
        gestionMenuCaballos();
        break;

    case 8:
        fusionaListaCaballos();
        gestionMenuCaballos();
        break;

    case 9:
        fusionaCaballos();
        gestionMenuCaballos();
        break;

    case 10:
        comparaCaballo();
        gestionMenuCaballos();
        break;

    case 11:
        caballosporedad();
        gestionMenuCaballos();
        break;


    default:

        MenuCaballos();
        gestionMenuCaballos();
        break;
    }
}
