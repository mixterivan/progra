#include<stdio.h>
#include<stdlib.h>
#include <windows.h>
#include "InterfazUsuario/interfazUsuario.h"
#include "InterfazUsuario/CaballoIU.h"
#include "InterfazUsuario/interfazGrafica.h"

int main()
{

    system("title Proyecto Integrador");
    system("mode con cols=80 lines=50");

    inicInterfazUsuario();

    gestionMenuPrincipal();

    return 0;
}
